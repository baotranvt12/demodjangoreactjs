import React, { Component } from 'react';

class Home extends Component {
    render() {
        return (
            <div>
                <header className="w3-container w3-center w3-padding-32"> 
                    <h1><b>Quản lý nhân viên</b></h1>
                    <p> TRUNG TÂM TIN HỌC TRƯỜNG ĐẠI HỌC SƯ PHẠM TP.HCM <span className="w3-tag">R&D PROJECT</span></p>
                    <img src="https://ak.picdn.net/shutterstock/videos/1068303734/thumb/1.jpg" alt=" " style={{width: '100%'}} />
                </header>
            </div>
        );
    }
}

export default Home;